# pre-commit hook to check that messages are updated

This hook is meant to be used with [pre-commit](https://github.com/pre-commit/pre-commit).

It will only work properly starting with django 4.1, as that is the first version
that does not update the `POT-Creation-Date` header when there are no other changes

## Usage

To your `.pre-commit-config.yaml` add hook config similar to this:

``` yaml
repos:
 - repo: https://gitlab.com/nils-van-zuijlen/django-ensure-makemessages
   rev: 0.2.0
   hooks:
    - id: check-messages-updated
      additional_dependencies: [django==4.1]
```

## Hook arguments

| Argument | Description | Default value |
| --- | --- | --- |
| `--exec-command` | Command to execute manage.py with (can be set to run in docker container, poetry shell, etc.) | `python3` |
| `--manage-path` | Path to manage.py file | `./manage.py` |
| `-a` | Args to pass to makemessages command, pass multiple time for multiple args | `--all` |

## Additional dependencies

If you to use hook in docker environment / virtual environment with django installed,
you need to pass the `--exec-command`.

For example to run in a poetry venv:

``` yaml
repos:
 - repo: https://gitlab.com/nils-van-zuijlen/django-ensure-makemessages
   rev: 0.2.0
   hooks:
    - id: check-messages-updated
      args: [--exec-command=poetry run python]

```

## Adding djangojs translations


``` yaml
repos:
 - repo: https://gitlab.com/nils-van-zuijlen/django-ensure-makemessages
   rev: 0.2.0
   hooks:
   - id: check-messages-updated
     additional_dependencies: [django==4.1]
   - id: check-messages-updated
     additional_dependencies: [django==4.1]
     args: [-a=--domain=djangojs, -a=--all]
     files: "\\.js$"
```

## License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
