# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import argparse
import subprocess  # nosec


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--manage-path", type=str, default="./manage.py")
    parser.add_argument("--exec-command", type=str, default="python3")
    parser.add_argument(
        "--additional-args", "-a", type=str, nargs="*", default=["--all"]
    )
    args = parser.parse_args()

    pre_command = ""
    if any([part in ["poetry", "pipenv"] for part in args.exec_command.split(" ")]):
        # unsetting hooks virtualenv path from env variables so it does
        # NOT get used when calling poetry or pipenv
        pre_command = "unset VIRTUAL_ENV; "

    subprocess.check_output(
        f"{pre_command}{args.exec_command} {args.manage_path} makemessages {' '.join(args.additional_args)}",
        shell=True,  # nosec  # all args are passed in by the people in charge of the repository
    ).decode("utf-8")
    return 0


if __name__ == "__main__":
    exit(main())
